import Vue from "vue";
import i18n from "vuex-i18n";

import router from "@/js/router";
import store from "@/js/store";
import English from "@/js/store/translations/english";

import App from "./App.vue";


Vue.use(i18n.plugin, store);
Vue.i18n.add("en", English);
Vue.i18n.set("en");

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount("#app");
