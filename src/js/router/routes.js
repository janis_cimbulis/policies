import Home from "@/ui/pages/home/Home.vue";
import Policy from "@/ui/pages/policy/Policy.vue";


export default [
    {
        path: "/",
        name: "home",
        component: Home,
    },
    {
        path: "/policy/:id",
        name: "policy",
        component: Policy,
    },
    {
        path: "*",
        redirect: "/",
    },
];
