export default {
    policyName: "Policy Name",
    insuredAmount: "Insured Amount",

    firstName: "First Name",
    lastName: "Last Name",
    birthDate: "Birth Date",

    year: "Year",

    destination: "Destination",
    numOfPersons: "Number of Persons",

    address: "Address",
    squareMeters: "Sq.m.",

    makeModel: "Make and Model",
    registrationNumber: "Registration Number",

    kind: "Kind",
    breed: "Breed",

    insurancePolicies: "Insurance Policies",

    travel: "Travel Insurance",
    pet: "Pet Insurance",
    kasko: "KASKO Insurance",
    property: "Property Insurance",
    bike: "Bike Insurance",

    hide: "Hide",
    showMore: "Show {count} more",

    save: "Save",
    cancel: "Cancel",
};
