import Vue from "vue";
import Vuex from "vuex";
import { VuexPersistence } from "vuex-persist";

import policiesModule from "@/js/store/policies/policiesModule.js";


Vue.use(Vuex);

const vuexPersist = new VuexPersistence({
    // reducer: state => state.auth,
    storage: sessionStorage,
    reducer: (state) => state,
});

export default new Vuex.Store({
    state: {},
    modules: [policiesModule],
    plugins: [vuexPersist.plugin],
});
