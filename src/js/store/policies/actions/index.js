import { SET_POLICY } from "@/js/store/policies/mutations";


export const UPDATE_POLICY = "policies/UPDATE_POLICY";

export const actions = {
    /**
     * @param commit
     * @param payload { Policy }
     */
    [UPDATE_POLICY]: ({ commit }, payload) =>
        commit(SET_POLICY, payload),
};
