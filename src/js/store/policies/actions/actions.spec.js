import Vuex from "vuex";
import { createLocalVue } from "@vue/test-utils";

import {
    actions,
    UPDATE_POLICY,
} from "@/js/store/policies/actions";
import { SET_POLICY } from "@/js/store/policies/mutations";


const localVue = createLocalVue();
localVue.use(Vuex);

describe("policies/index", () => {
    let store;
    let mockSetPolicy;

    beforeEach(() => {
        mockSetPolicy = jest.fn();

        store = new Vuex.Store({
            state: {},
            actions: {
                [UPDATE_POLICY]: actions[UPDATE_POLICY],
            },
            mutations: {
                [SET_POLICY]: mockSetPolicy,
            },
        });
    });

    it("should commit SET_POLICY mutation", () => {
        store.dispatch(UPDATE_POLICY);
        expect(mockSetPolicy).toHaveBeenCalled();
    });
});
