import {
    mutations,
    SET_POLICY,
} from "./index.js";

import { Owner, PetPolicy } from "@/js/store/policies/Policy.js";


jest.mock("@/js/services/guid.js", () =>
    ({
        GUID: jest.fn()
            .mockImplementation(() => Promise.resolve("1")),
    }));

describe("policies/mutations", () => {
    const thePolicy = new PetPolicy(
        new Owner("J", "C", "23-07-1991"),
        1000,
        "dog",
        "German Shepherd",
        2019,
    );

    const policyToSet = {
        ...thePolicy,
        data: { insuredAmount: 100 },
        meta: { kind: "cat", breed: "British Shorthair" },
    };

    let state;

    beforeEach(() => {
        state = {
            policies: [thePolicy],
        };
    });

    it("should set notification", () => {
        expect(state.policies).toEqual([thePolicy]);
        mutations[SET_POLICY](state, policyToSet);
        expect(state.policies).toEqual([policyToSet]);
    });
});
