import Vue from "vue";


export const SET_POLICY = "policies/SET_POLICY";

export const mutations = {
    [SET_POLICY]: (state, policy) => {
        Vue.set(
            state.policies,
            state.policies
                .findIndex((p) => p.id === policy.id),
            policy,
        );
    },
};
