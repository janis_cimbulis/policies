// eslint-disable-next-line max-classes-per-file
import { GUID } from "@/js/services/guid.js";


export class Owner {
    /**
     * @param firstName
     * @param lastName
     * @param birthDate
     */
    constructor(
        firstName,
        lastName,
        birthDate,
    ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
    }
}

class Policy {
    /**
     * @param data { { owner }, insuredAmount }
     * @param meta - depends on policy type
     */
    constructor(
        data = {
            owner: null,
            insuredAmount: 0,
            name: null,
        },
        meta = null,
    ) {
        this.id = GUID();
        this.data = data;
        this.meta = meta;
    }


}


export class TravelPolicy extends Policy {
    /**
     * @param owner
     * @param insuredAmount
     * @param name
     * @param destination
     * @param numOfPersons
     */
    constructor(
        owner,
        insuredAmount,
        name,
        destination,
        numOfPersons,
    ) {
        super(
            { owner, insuredAmount, name },
            { destination, numOfPersons },
        );
    }
}

export class PropertyPolicy extends Policy {
    /**
     * @param owner
     * @param insuredAmount
     * @param name
     * @param address
     * @param squareMeters
     */
    constructor(
        owner,
        insuredAmount,
        name,
        address,
        squareMeters,
    ) {
        super(
            { owner, insuredAmount, name },
            { address, squareMeters },
        );
    }
}

export class VehiclePolicy extends Policy {
    /**
     * @param owner
     * @param insuredAmount
     * @param name
     * @param makeModel
     * @param year
     * @param registrationNumber
     */
    constructor(
        owner,
        insuredAmount,
        name,
        makeModel,
        year,
        registrationNumber,
    ) {
        super(
            { owner, insuredAmount, name },
            { makeModel, year, registrationNumber },
        );
    }
}

export class PetPolicy extends Policy {
    /**
     * @param owner
     * @param insuredAmount
     * @param name
     * @param kind
     * @param breed
     * @param year
     */
    constructor(
        owner,
        insuredAmount,
        name,
        kind,
        breed,
        year,
    ) {
        super(
            { owner, insuredAmount, name },
            { kind, breed, year },
        );
    }
}
