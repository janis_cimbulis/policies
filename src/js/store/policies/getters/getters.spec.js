import { getters, ALL_POLICIES, POLICY_BY_ID } from "./index.js";
import { Owner, PetPolicy } from "@/js/store/policies/Policy.js";


jest.mock("@/js/services/guid.js", () =>
    ({
        GUID: jest.fn()
            .mockImplementation(() => Promise.resolve("1")),
    }));


describe("policies/getters", () => {
    let thePolicy;
    let state;

    beforeEach(() => {
        thePolicy = new PetPolicy(
            new Owner("J", "C", "23-07-1991"),
            1000,
            "dog",
            "German Shepherd",
            2019,
        );

        state = {
            policies: [thePolicy],
        };
    });

    it("should return all policies", () =>
        expect(getters[ALL_POLICIES](state))
            .toEqual([thePolicy]));
});
