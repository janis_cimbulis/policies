export const ALL_POLICIES = "policies/ALL_POLICIES";
export const POLICY_BY_ID = "policies/POLICY_BY_ID";

export const getters = {
    /**
     * @param state
     * @returns { Array<Policy> }
     */
    [ALL_POLICIES]: (state) => state.policies,
    [POLICY_BY_ID]: (state) => (id) =>
        state.policies.find((p) => p.id === id),
};
