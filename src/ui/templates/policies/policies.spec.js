// TODO:
// validate props
// assert amount of items rendered

import {
    shallowMount,
    createLocalVue,
} from "@vue/test-utils";

import Vuex from "vuex";
import i18n from "vuex-i18n/dist/vuex-i18n.umd.js";

import English from "@/js/store/translations/english";

import Policies from "@/ui/templates/policies/Policies.vue";
import { initialPolicyList } from "@/js/store/policies/policiesModule.js";


const localVue = createLocalVue();
localVue.use(Vuex);

describe("Policies.vue", () => {
    let wrapper;
    const store = new Vuex.Store({});
    const mockPropsDataValid = initialPolicyList;
    const mockPropsDataInvalid = { invalid: 123 };

    localVue.use(i18n.plugin, store);
    localVue.i18n.add("en", English);
    localVue.i18n.set("en");

    it("should not throw on valid props", () => {
        wrapper = shallowMount(Policies, {
            store,
            localVue,
            propsData: {
                policies: mockPropsDataValid,
            },
        });
        const policiesProp = wrapper.vm.$options.props.policies;

        expect(policiesProp.required)
            .toBe(true);
    });

    it("should throw on invalid props", () => {
        const spy = jest.spyOn(console, "error")
            .mockImplementation(() => {
            });

        wrapper = shallowMount(Policies, {
            store,
            localVue,
            propsData: {
                policies: mockPropsDataInvalid,
            },
        });

        const expectedError = expect.stringContaining(
            "[Vue warn]: Invalid prop: type check failed for prop \"policies\"",
        );
        expect(spy).toBeCalledWith(expectedError);
    });
});
