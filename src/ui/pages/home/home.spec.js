import { shallowMount } from "@vue/test-utils";

import Home from "./Home.vue";


describe("App", () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallowMount(Home, {});
    });

    it("should render home page", () => {
        expect(wrapper.contains("#if-policy-list")).toBeTruthy();
    });
});
