module.exports = {
    root: true,
    env: {
        node: true,
        "jest/globals": true,
        browser: true,
    },
    extends: [
        "eslint:recommended",
        "plugin:vue/essential",
        "@vue/airbnb",
    ],
    plugins: [
        "vue",
        "jest",
    ],
    settings: {
        "import/resolver": {
            node: {
                paths: ["src"],
            },
        },
    },
    rules: {
        "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
        "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
        indent: ["error", 4, { SwitchCase: 1 }],
        quotes: ["error", "double"],
        "import/extensions": 0,
        "prefer-promise-reject-errors": 0,
        "consistent-return": 0,
        "no-else-return": 0,
        "implicit-arrow-linebreak": 0,
        "no-bitwise": 0,
        "template-curly-spacing": 0,
        "padded-blocks": 0,
        "brace-style": 0,
        "no-param-reassign": 0,
        "func-names": 0,
        "operator-linebreak": 0,
    },
    parserOptions: {
        parser: "babel-eslint",
    },
};
